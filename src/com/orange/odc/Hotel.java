package com.orange.odc;

import java.util.ArrayList;
import java.util.Date;

public class Hotel {

    //Les propriétés de la classe
    private int hotelId;
    private String nom;
    private String localisation;

    private int nbreEtoile ;
    private int nbreEmploye;

    ArrayList<Chambre> chambres;
    ArrayList<Reservation> reservations;
    public ArrayList<Client> clientsAyantNote;

    Hotel(){
        this.chambres = new ArrayList<>();
        this.reservations = new ArrayList<>();
    }

    Hotel(String name, String localisation , int stars, int resource) {
        this();
        this.nom = name;
        this.localisation = localisation;
        this.nbreEtoile = stars;
        this.nbreEmploye = resource;

    }

    @Override
    public String toString() {
        return "Nom: " + this.nom + " \n" +
                "Localisation: " + this.localisation + " \n" +
                "Nombre d'étoiles: " + this.nbreEtoile + " \n" +
                "Employes: " + this.nbreEmploye + " \n";
    }


    /**
     *
     * @param chambre
     * add room in arraylist of room
     */
    public void addRoom(Chambre chambre) {
        if(!checkIfRoomExistInHotel(chambre))
            this.chambres.add(chambre);
        else
            System.out.println("Impossible");
    }

    public int getTotalRoom() {
        return chambres.size();
    }

    public int getTotalRoomAvailable() {
        int compteur = 0;
        for (Chambre chambre : chambres) {
            if(chambre.isDisponible()) compteur++;
        }
        return compteur;
    }


    public boolean checkIfRoomExistInHotel(Chambre chambre) {
        for(Chambre element : chambres) {
            if(element.getNumeroChambre() == chambre.getNumeroChambre())
                return true;
        }
        return false;
    }

    //Calcul du chiffre d'affaire de l'hotel en fonction des réservations.
    public double chiffreAffaire(){
        double chiffreAffaire= 0;
        for (Reservation reservation : reservations) {
           chiffreAffaire += reservation.getPrixReservation();
        }
        return chiffreAffaire;
    }
    //Annulation de la reservation par l'hotel
    public boolean annulerReservation(Reservation reservation){
        ArrayList<Reservation> newResevationList = new ArrayList<>();
        for (Reservation element:reservations) {
            if(!element.equals(reservation)){
                newResevationList.add(element);
            }
        }
        reservation.cancelReservation();
        this.reservations = newResevationList;
        return true;
    }

    //Possibilité d'étendre la période de réservation fait par l'hotel
    public void etendrePeriodeReservation(Reservation reservation, Date newEndReservationDate){
        if (reservation.etendrePeriodeReservation(newEndReservationDate))
            System.out.println("la reservation de "+reservation.client.name+" vient d'être étendu au "+reservation.endDate);
    }



    public void createReservation(String standing, Client client,  Date date) {
        //TODO Recuperer la liste des chambres de même standing
        // Parcours les reservations et voir si les chambres y sont

        //TODO Stéphane
        /*Chambre chambre = getRoomAvailableByStanding(standing);
        if(chambre != null) {
            Reservation reservation = new Reservation(chambre, client, date);
            chambre.changeState();
            addReservation(reservation);
        }else{
            System.out.println("Reservation impossible. Aucune chambre n'est disponible");
        }*/

        Chambre chambre = getRoomAvailableByStanding("VIP");
        if(chambre != null) {
            System.out.println(chambre.toString());
            Reservation reservation = new Reservation(chambre, client, date);
            chambre.changeState();
            addReservation(reservation);

        }else{
            System.out.println("null");
        }
    }

    private Chambre getRoomAvailableByStanding(String standing) {
        for (Chambre chambre : chambres) {
            if(chambre.getStanding().equals(standing) && chambre.isDisponible())
                return chambre;
        }
        return null;
    }

    private void addReservation(Reservation reservation) {
        this.reservations.add(reservation);
    }


    public ArrayList<Client> getAllClient() {
        ArrayList<Client> clients = new ArrayList<Client>();
        for (Reservation reservation : reservations)
            clients.add(reservation.client);

        return clients;
    }


    public Chambre getMinDate() {
        Chambre chambre = this.reservations.get(0).chambre;
        Date minDate = this.reservations.get(0).endDate;


        for (Reservation reservation : reservations) {
            System.out.println("Comparaison :" + minDate.compareTo(reservation.endDate));
            if(minDate.compareTo(reservation.endDate) >= 0){
                chambre = reservation.chambre;
                minDate = reservation.endDate;
            }
        }

        return chambre;
    }

    //Mise à jours automatique du nombre d'étoile de l'hotel et prise en considération de la note d'un client.
    public void addNoteClient(Client client, int nbreEtoile) {
        for (Client element:clientsAyantNote) {
            if(element.equals(client)){
                System.out.println("Désolé vous ne pouvez noter qu'une seule fois");
                return;
            }
            this.setNbreEtoile((int)((this.nbreEtoile*clientsAyantNote.size()+nbreEtoile)/(clientsAyantNote.size()+1)));
            clientsAyantNote.add(client);
        }
    }


    public int getHotelId() {
        return hotelId;
    }

    public void setHotelId(int hotelId) {
        this.hotelId = hotelId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLocalisation() {
        return localisation;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public int getNbreEtoile() {
        return nbreEtoile;
    }

    public void setNbreEtoile(int nbreEtoile) {
        this.nbreEtoile = nbreEtoile;
    }

    public int getNbreEmploye() {
        return nbreEmploye;
    }

    public void setNbreEmploye(int nbreEmploye) {
        this.nbreEmploye = nbreEmploye;
    }

}
