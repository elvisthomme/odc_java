package com.orange.odc;

import java.util.Date;

public class Reservation {
    public Chambre chambre;
    public Client client;
    public Date startDate;
    public Date endDate;

    public Reservation(Chambre chambre, Client client, Date endDate) {
        this.chambre = chambre;
        this.client = client;
        this.startDate = new Date();
        this.endDate = endDate;
    }

    //Calcul du montant à payer lors de la reservation.
    public double getPrixReservation(){
        double dureeReservationEnMilisSecondes = this.endDate.getTime() - this.startDate.getTime();
        return (dureeReservationEnMilisSecondes/(1000*60*60*24))*this.chambre.getPrix();
    }

    //Annulation de la reservation
    public void cancelReservation(){
        this.chambre.changeState();
        this.chambre = new Chambre();
        this.client = new Client();
    }

    //Possibilité d'étendre la periode de Réservation
    public boolean etendrePeriodeReservation(Date newEndDate){
        if(this.endDate.getTime()<newEndDate.getTime()){
            this.endDate = newEndDate;
            return true;
        }
        System.out.println("Désolé prolongement échoué choisissez une date plus éloigné que "+ this.endDate);
        return false;
    }
}
