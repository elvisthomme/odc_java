package com.orange.odc;

public class Flight {
    private int nbrePassager;
    private int nbreKilo;
    private String identifiant;

    private final int NBRE_PASSAGER_MAX = 100;
    private final int NBRE_KILO_MAX = 3600;

    Flight(String identifiant) {
        this.identifiant = identifiant;
    }

    Flight(String identifiant, int nbrePassager, int kilo) {
        this.identifiant = identifiant;
        this.nbrePassager = nbrePassager;
        this.nbreKilo = kilo;
    }

    public void addPassenger(int kilo) {
        int totalKilo = nbreKilo + kilo;
        if(totalKilo <= NBRE_KILO_MAX && nbrePassager+1 <= NBRE_PASSAGER_MAX) {
            this.nbrePassager++;
            this.setNbreKilo(totalKilo);
        }
    }


    public boolean transfert(Flight flight) {
        System.out.println(this.toString());
        System.out.println("----------------------------");
        System.out.println(flight.toString());
        boolean canAddPassenger = this.nbrePassager + flight.nbrePassager <= NBRE_PASSAGER_MAX ? true : false;
        boolean canTakeKilo = this.nbreKilo + flight.nbreKilo <= NBRE_KILO_MAX ? true : false;


        boolean result =  canAddPassenger && canTakeKilo;
        if(result) {
            flight.setNbrePassager(this.nbrePassager + flight.nbrePassager);
            flight.setNbreKilo(this.nbreKilo + flight.nbreKilo);

            this.setNbreKilo(0);
            this.setNbrePassager(0);
            System.out.println(flight.toString());
            return true;
        }
        return false;
    }


    @Override
    public String toString() {
        return "Identifiant : " + this.identifiant + "\n" +
                "Nbre de passagers : " + this.nbrePassager + "\n" +
                "Nbre de Kilo : " + this.nbreKilo + "\n";
    }

    public int getNbrePassager() {
        return nbrePassager;
    }

    public void setNbrePassager(int nbrePassager) {
        this.nbrePassager = nbrePassager;
    }

    public int getNbreKilo() {
        return nbreKilo;
    }

    public void setNbreKilo(int nbreKilo) {
        this.nbreKilo = nbreKilo;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public int getNBRE_PASSAGER_MAX() {
        return NBRE_PASSAGER_MAX;
    }

    public int getNBRE_KILO_MAX() {
        return NBRE_KILO_MAX;
    }
}
