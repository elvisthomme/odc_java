package com.orange.odc;

import java.util.Date;

public class Main {

    public static void main(String[] args) {

        Hotel akwaPalace = new Hotel("Akwa Palace", "Douala Akwa", 3, 120);
        int room = akwaPalace.getTotalRoom();


        Chambre chambreStandard = new Chambre(1, "Standard", 15000);

        chambreStandard.changeState();
        Chambre chambreVIP = new Chambre(4, "VIP", 45000);
        chambreVIP.changeState();

        Chambre chambreVIP2 = new Chambre(3, "VIP", 45000);

        akwaPalace.addRoom(chambreStandard);
        akwaPalace.addRoom(chambreVIP);
        chambreVIP2.changeState();
        akwaPalace.addRoom(chambreVIP2);

        Client soubiel = new Client("Soubiel", "6997VVVV");

       //Reservation reservationOne = new Reservation(chambreStandard, soubiel, new Date());



        akwaPalace.createReservation("VIP", soubiel, new Date(2020, 11, 22));
        akwaPalace.createReservation("VIP", soubiel, new Date(2020, 12, 31));
        akwaPalace.createReservation("Standard", soubiel, new Date(2020, 12, 15));


        System.out.println("Prochaine chambre disponible " + akwaPalace.getMinDate().toString());



        /*

        Flight volOne = new Flight("VOL-1", 45, 3200);
        System.out.println(volOne.getIdentifiant());


        Flight flightTwo = new Flight("VOL-2",
                10, 230);


        flightTwo.addPassenger(45);

        flightTwo.transfert(volOne);

        */


        //TODO Créer une méthode qui dit si une chambre est occupée ou non

        //TODO Compter toutes les chambres disponibles dans un hotel

        //TODO Créer une classe Client et ses attributs

        //TODO Créer une classe Reservation et ses attributs.
            // Client
            // Chambre
            // Date de début de la reservation
            // Date de fin de la reservation

        //TODO Ecrire une méthode permettant à un client d'effectuer une reservation

        //TODO Ecrire une méthode qui liste les différents clients actuellement dans l'hotel

        //TODO Ecrire une méthode qui donne la chambre qui se liberera le plus tôt





    }
}
