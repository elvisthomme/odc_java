package com.orange.odc;

import java.util.Date;

public class Client {
    public String name;
    public String phoneNumber;


    public Client(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }
    public Client(){}

    //Annulation de la reservation par le client.
    public boolean annulerReservation(Reservation reservation, Hotel hotel){
        hotel.annulerReservation(reservation);
        System.out.println("Reservation annuler avec succes par "+this.name);
        return true;
    }

    //Possibilité d'étendre la période de réservation par le client
    public void etendrePeriodeReservation(Hotel hotel, Reservation reservation, Date newEndReservationDate){
        hotel.etendrePeriodeReservation(reservation, newEndReservationDate);
        System.out.println("vous venez d'étendre votre réservation pour le "+ reservation.endDate);
    }

    //Possibilité de noter l'hotel
    //À chaque fois qu'une note est donné à l'hotel par le client,
    public void noterHotel(Hotel hotel, int nbreEtoile){
        if (nbreEtoile>0 && nbreEtoile<5) {
            hotel.addNoteClient(this, nbreEtoile);
        }
    }
}
