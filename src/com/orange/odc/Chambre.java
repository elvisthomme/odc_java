package com.orange.odc;

public class Chambre {

    private int numeroChambre;
    private String standing;
    private double prix;
    private boolean disponible;

    public Chambre(){}

    public Chambre(int numeroChambre, String standing, double prix) {
        this.numeroChambre = numeroChambre;
        this.standing = standing;
        this.prix = prix;
    }

    public int getNumeroChambre() {
        return numeroChambre;
    }

    public boolean isDisponible() {
        return disponible;
    }


    public void changeState() {
        disponible = !disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public void setNumeroChambre(int numeroChambre) {
        this.numeroChambre = numeroChambre;
    }

    public String getStanding() {
        return standing;
    }

    public void setStanding(String standing) {
        this.standing = standing;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        String disponible = this.disponible  ? "OK" : "NON";
        return "Chambre : " + this.standing + " \n" +
                "Numero : " + this.numeroChambre + " \n" +
                "Disponibilite:  " + disponible;
    }
}
